
package modelo;

import java.util.HashSet; 
import java.util.StringTokenizer;
import java.io.*;

/**
 * Fichero ModeloFichero.java
 * @author Ignacio Rodríguez <nachorodriguez71@gmail.com>
 * @date 22-ene-2015
 *  
 */
public class ModeloFichero implements IModelo{
    private File alumnos;
    private File grupos;
    
    /* Usaremos un par de HashSets para saber en todo momento las
    ids guardadas en el fichero y, de esta manera, no acceder a este
    en caso de que no sea necesario, por ejemplo si el usuario trata
    de actualizar o borrar un registro inexistente */
    private HashSet indicesA;
    private HashSet indicesG;
    
    private int ida = 0;
    private int idg = 0;

    public ModeloFichero() throws IOException {
        indicesA=new HashSet();
        indicesG=new HashSet();

        alumnos = new File("alumnos.csv");
        grupos = new File("grupos.csv");

        // Si no existen los ficheros los creamos por primera vez
        if (!alumnos.exists()) {
            FileWriter fwa = new FileWriter(alumnos);
            fwa.close();
        } else {
            /* Vamos a rellenar el HashSet indicesA con los íds de los
            alumnos y actualizaremos el contador ida con el valor del
            mayor id más uno. */
            actualizaIndicesA();

            /*Antes podríamos crear una función que comprobara si el
            fichero tiene o no un formato correcto y/o datos válidos.
            Lo que sí permito (añadiendo un poco más de código en los
            métodos) es que hayan líneas en blanco, aunque si no se 
            toca a mano el fichero no deberían haber y cuando se 
            borra un elemento se borran también las líneas en blanco*/
        }
        if (!grupos.exists()) {
            FileWriter fwg = new FileWriter(grupos);
            fwg.close();
        } else {
            //Ahora hacemos lo mismo con los grupos
            actualizaIndicesG();
        }
    }
    
    @Override
    public void create(Alumno alumno) throws IOException {
        FileWriter fw;
        PrintWriter pw;
        String cadena;

        fw=new FileWriter(alumnos, true);
        pw=new PrintWriter(fw);
        alumno.setId("" + ida);
        cadena=creaCadenaAlumno(alumno);
        pw.println(cadena);
        pw.close();
        fw.close();
        this.indicesA.add("" + ida);
        ida++;
    }

    @Override
    public void update(Alumno alumno) throws IOException {
        File temp;
        FileWriter fw;
        PrintWriter pw;
        FileReader fr;
        BufferedReader br;
        StringTokenizer st;
        String idA, linea;

        //Si no existe el índice del alumno no hacemos nada
        if (indicesA.contains(alumno.getId())) {
            /* En este archivo temporal vamos copiando línea a línea
            el archivo original excepto la línea que actualizamos que
            escribimos la línea actualizada */
            temp = new File("temp");

            fw = new FileWriter(temp, true);
            pw = new PrintWriter(fw);
            fr = new FileReader(alumnos);
            br = new BufferedReader(fr);

            linea = br.readLine();
            while (linea != null) {
                //Nos saltamos posibles líneas en blanco
                if (linea.trim().equals("")) {
                    linea= br.readLine();
                    continue;
                }
                st=new StringTokenizer(linea,";");
                idA=st.nextToken();
                //cuando lo encontremos escribimos el actualizado
                if (alumno.getId().equals(idA)) {
                    pw.println(creaCadenaAlumno(alumno));
                } else {
                    //si no escribimos lo que había
                    pw.println(linea);
                }
                linea = br.readLine();
            }

            br.close();
            fr.close();
            pw.close();
            fw.close();

            // Una vez construido el nuevo archivo borramos el original
            // y renombramos el nuevo.
            alumnos.delete();
            boolean okRenombra = temp.renameTo(new File("alumnos.csv"));
        } 
    }

    @Override
    public void delete(Alumno alumno)  throws IOException {
        File temp;
        FileWriter fw;
        PrintWriter pw;
        FileReader fr;
        BufferedReader br;
        StringTokenizer st;
        String idA, linea;

        //Si no existe el índice del alumno no hacemos nada
        if (indicesA.contains(alumno.getId())) {
            /* En este archivo temporal vamos copiando línea a línea
            el archivo original excepto la línea que queremos borrar */
            temp = new File("temp");

            fw = new FileWriter(temp, true);
            pw = new PrintWriter(fw);
            fr = new FileReader(alumnos);
            br = new BufferedReader(fr);

            linea = br.readLine();
            while (linea != null) {
                //Nos saltamos posibles líneas en blanco
                if (linea.trim().equals("")) {
                    linea= br.readLine();
                    continue;
                }
                st=new StringTokenizer(linea,";");
                idA=st.nextToken();

                /* mientras no coincida con el que queremos borrar vamos
                  copiando línea a linea. Si encontramos el que vamos a
                  borrar simplemente nos lo saltamos, no hacemos nada */
                if (!alumno.getId().equals(idA)) {
                    pw.println(linea);
                }
                linea = br.readLine();
            }

            br.close();
            fr.close();
            pw.close();
            fw.close();

            // Una vez construido el nuevo archivo borramos el original
            // y renombramos el nuevo.
            alumnos.delete();
            boolean okRenombra = temp.renameTo(new File("alumnos.csv"));
            //Actualizamos el hash con los índices
            indicesA.remove(alumno.getId());
        } 
    }
    
    @Override
    public HashSet<Alumno> reada() throws IOException {
        FileReader fr;
        BufferedReader br;
        StringTokenizer st;
        String idA, nombreA, email, idG, cadena;
        int edad;
        Alumno al;
        Grupo gr;

        HashSet hs = new HashSet();
        fr = new FileReader(alumnos);
        br = new BufferedReader(fr);
        cadena = br.readLine();
        while (cadena != null) {
            // Nos saltamos posibles líneas en blanco
            if (cadena.trim().equals("")) {
                cadena= br.readLine();
                continue;
            }
            st = new StringTokenizer(cadena, ";");
            idA = st.nextToken();
            nombreA = st.nextToken();
            edad = Integer.parseInt(st.nextToken());
            email = st.nextToken();
            idG= st.nextToken();
            al = new Alumno();
            al.setId(idA);
            al.setNombre(nombreA);
            al.setEdad(edad);
            al.setEmail(email);
            gr=obtenerGrupo(idG);
            al.setGrupo(gr);
            hs.add(al);
            cadena = br.readLine();
        }
        br.close();
        fr.close();
        return hs;
    }

    @Override
    public void create(Grupo grupo) throws IOException {
        FileWriter fw;
        PrintWriter pw;

        fw=new FileWriter(grupos, true);
        pw=new PrintWriter(fw);
        pw.println("" + idg + ";" + grupo.getNombre());
        grupo.setId("" + idg);
        pw.close();
        fw.close();
        this.indicesG.add("" + idg);
        idg++;
    }

    @Override
    public void update(Grupo grupo) throws IOException {
        File temp;
        FileWriter fw;
        PrintWriter pw;
        FileReader fr;
        BufferedReader br;
        StringTokenizer st;
        String idG, cadena;

        //Si no existe el índice del grupo no hacemos nada
        if (indicesG.contains(grupo.getId())) {

            /* En este archivo temporal vamos copiando línea a línea
            el archivo original excepto la línea que actualizamos que
            escribimos la línea actualizada */
            temp = new File("temp");

            fw = new FileWriter(temp, true);
            pw = new PrintWriter(fw);
            fr = new FileReader(grupos);
            br = new BufferedReader(fr);

            cadena = br.readLine();
            while (cadena != null) {
                //Nos saltamos posibles líneas en blanco
                if (cadena.trim().equals("")) {
                    cadena= br.readLine();
                    continue;
                }
                st=new StringTokenizer(cadena,";");
                idG=st.nextToken();
                //cuando lo encontremos escribimos el actualizado
                if (grupo.getId().equals(idG)) {
                    pw.println("" + grupo.getId() + ";" + grupo.getNombre());
                } else {
                    //si no escribimos lo que había
                    pw.println(cadena);
                }
                cadena = br.readLine();
            }

            br.close();
            fr.close();
            pw.close();
            fw.close();
                
            // Una vez construido el nuevo archivo borramos el original
            // y renombramos el nuevo.
            grupos.delete();
            boolean okRenombra = temp.renameTo(new File("grupos.csv"));
        }
    }

    @Override
    public void delete(Grupo grupo) throws IOException {
        File temp;
        FileWriter fw;
        PrintWriter pw;
        FileReader fr;
        BufferedReader br;
        StringTokenizer st;
        String idG, cadena;

        //Si no existe el índice del grupo no hacemos nada
        if (indicesG.contains(grupo.getId())) {

            /* En este archivo temporal vamos copiando línea a línea
            el archivo original excepto la línea que queremos borrar */
            temp = new File("temp");

            fw = new FileWriter(temp, true);
            pw=new PrintWriter(fw);
            fr = new FileReader(grupos);
            br = new BufferedReader(fr);

            cadena = br.readLine();

            while (cadena != null) {
                //Nos saltamos posibles líneas en blanco
                if (cadena.trim().equals("")) {
                    cadena= br.readLine();
                    continue;
                }
                st=new StringTokenizer(cadena,";");
                idG=st.nextToken();
                // Mientras no lo encontremos vamos copiando las líneas
                // Si lo encontramos nos lo saltamos, no hacemos nada
                if (!grupo.getId().equals(idG)) {
                    pw.println(cadena);
                }
                cadena = br.readLine();
            }

            br.close();
            fr.close();
            pw.close();
            fw.close();

            // Una vez construido el nuevo archivo borramos el original
            // y renombramos el nuevo.
            grupos.delete();
            boolean okRenombra = temp.renameTo(new File("grupos.csv"));
            //Actualizamos el hash con los índices
            indicesG.remove(grupo.getId());
        }
    }

    @Override
    public HashSet<Grupo> readg() throws IOException {
        FileReader fr;
        BufferedReader br;
        StringTokenizer st;
        String idG, nombre, cadena;
        Grupo gr;

        HashSet hs = new HashSet();
        fr = new FileReader(grupos);
        br = new BufferedReader(fr);
        cadena = br.readLine();
        while (cadena != null) {
            // Nos saltamos posibles líneas en blanco
            if (cadena.trim().equals("")) {
                cadena= br.readLine();
                continue;
            }
            st = new StringTokenizer(cadena, ";");
            idG = st.nextToken();
            nombre = st.nextToken();
            gr = new Grupo();
            gr.setId(idG);
            gr.setNombre(nombre);
            hs.add(gr);
            cadena = br.readLine();
        }
        br.close();
        fr.close();
        
        return hs;
    }

    // Main para hacer pruebas
    public static void main (String[] args) throws IOException {
        
    }

    private void actualizaIndicesA()  throws IOException {
        FileReader fr;
        BufferedReader br;
        int pos, max=0, idNum;
        String linea, id;
        boolean añadido=false;

        fr=new FileReader(alumnos);
        br=new BufferedReader(fr);
        linea=br.readLine();
        while(linea !=null) {
            // Busco la posición del primer ";"
            pos=linea.indexOf(";"); 
            // Evitamos líneas erróneas o borradas a mano
            if (pos==-1) {
                linea=br.readLine();
                continue;
            } 
            id=linea.substring(0,pos);
            idNum=Integer.parseInt(id);
            boolean ok = indicesA.add(id); //Añadimos el índice
             // Si se añade aunque sea un índice marcamos añadido
            if (ok) {añadido=true;}
            if (idNum>max) {max=idNum;}
            linea=br.readLine();
        }
        /* Esto es necesario por si el fichero está vacío y no
        se añade nada para que no incremente ida y siga teniendo 
        el valor inicial */
        if (añadido) {this.ida=max+1;} //Actualizamos ida

        br.close();
        fr.close();
    }

    private void actualizaIndicesG() throws IOException {
        FileReader fr;
        BufferedReader br;
        int pos, max=0, idNum;
        String linea, id;
        boolean añadido=false;

        fr=new FileReader(grupos);
        br=new BufferedReader(fr);
        linea=br.readLine();
        while(linea !=null) {
             // Busco la posición del primer ";"
            pos=linea.indexOf(";"); 
            // Evitamos líneas erróneas o borradas a mano
            if (pos==-1) {
                linea=br.readLine();
                continue;
            } 
            id=linea.substring(0,pos);
            idNum=Integer.parseInt(id);
            boolean ok = indicesG.add(id); //Añadimos el índice
             // Si se añade aunque sea un índice marcamos añadido
            if (ok) {añadido=true;}
            if (idNum>max) {max=idNum;}
            linea=br.readLine();
        }
        /* Esto es necesario por si el fichero está vacío y no
        se añade nada para que no incremente ida y siga teniendo 
        el valor inicial */
        if (añadido) {this.idg=max+1;} //Actualizamos idg
        br.close();
        fr.close();
    }

    private String creaCadenaAlumno(Alumno a) {
        String cadena;
        cadena=a.getId() + ";";
        cadena+=a.getNombre() + ";";
        cadena+=String.valueOf(a.getEdad()) + ";";
        cadena+=a.getEmail()+";";
        cadena+=a.getGrupo().getId();
        return cadena;
    }
    
    // Lee el fichero de grupos y devuelve un grupo a partir de un id
    private Grupo obtenerGrupo(String g)  throws IOException {
        FileReader fr;
        BufferedReader br;
        StringTokenizer st;
        String idG, nombre, cadena;
        Grupo gr=null;
        Boolean encontrado;

        encontrado=false;  
        fr = new FileReader(grupos);
        br = new BufferedReader(fr);
        cadena = br.readLine();
        while (cadena != null && !encontrado) {
            // Nos saltamos posibles líneas en blanco
            if (cadena.trim().equals("")) {
                cadena= br.readLine();
                continue;
            }
            st = new StringTokenizer(cadena, ";");
            idG = st.nextToken();
            if (idG.equals(g)) {
                encontrado=true;
                nombre=st.nextToken();
                gr=new Grupo();
                gr.setId(idG);
                gr.setNombre(nombre);
            } else {
                cadena = br.readLine();
            }
        }
        br.close();
        fr.close();
  
        return gr;
    }
}
