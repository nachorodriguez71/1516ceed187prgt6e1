/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.IOException;
import java.util.HashSet;

/**
 *
 * @author Ignacio Rodríguez <nachorodriguez71@gmail.com>
 */
public interface IModelo {

    // Alumno
    public void create(Alumno a) throws IOException; // Crea uno nuevo

    public void update(Alumno a) throws IOException; // Actuzaliza uno

    public void delete(Alumno a) throws IOException;  // Borrar uno

    public HashSet<Alumno> reada() throws IOException; // Obtiene todos

    // Grupo
    public void create(Grupo g) throws IOException; // Crea uno nuevo

    public void update(Grupo g) throws IOException; // Actuzaliza uno

    public void delete(Grupo g) throws IOException; // Borrar uno

    public HashSet<Grupo> readg() throws IOException; // Obtiene todos

}
