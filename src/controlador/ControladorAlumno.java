
package controlador;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import modelo.Alumno;
import modelo.Grupo;
import modelo.IModelo;
import vista.IVista;
import vista.Vista;

/**
 * @author Ignacio Rodríguez <nachorodriguez71@gmail.com>
 */
public class ControladorAlumno {

    IModelo modelo;
    IVista<Alumno> vistaAlumno;
    Alumno alumno = new Alumno();
    Grupo grupo = new Grupo();
    Vista vista = new Vista();

    ControladorAlumno(IModelo modelo, IVista<Alumno> vistaAlumno) throws IOException {
        this.modelo = modelo;
        this.vistaAlumno = vistaAlumno;
        menuCrud();
    }

    private void menuCrud() throws IOException {

        char opcion = ' ';
        do {
            vista.mostrarTexto("\nALUMNO");
            opcion = vista.menuCrud();
            switch (opcion) {
                case 'e': // Exit
                    vista.exit();
                    break;

                case 'c': // Create
                    crear();
                    break;

                case 'r': // Read alumno
                    try {
                        vistaAlumno.mostrar(modelo.reada());
                    } catch (IOException ex) {
                        vista.mostrarError("Error de Entrada/Salida");
                    }
                    break;

                case 'u':  // Actualizar
                    actualizar();
                    break;

                case 'd': // Borrar
                    borrar();
                    break;
                default:
                    vista.mostrarError("Opción Incorrecta");
                    break;
            }

        } while (opcion != 'e');

    }

    private Grupo getGrupo(String id) {

        Grupo encontrado = null;
        HashSet grupos;
        try {
            grupos = modelo.readg();
            Iterator it = grupos.iterator();
            while (it.hasNext()) {
                Grupo grupo = (Grupo) it.next();
                if (id.equals(grupo.getId())) {
                    encontrado = grupo;
                }
            }
        } catch (IOException ex) {
            vista.mostrarError("Error de Entrada/Salida");
        }
        return encontrado;
    }

    private void crear() {
        Alumno alumno = null;
        Grupo grupo = null;

        alumno = vistaAlumno.obtener();
        grupo = getGrupo(alumno.getGrupo().getId());

        if (grupo != null) {
            alumno.setGrupo(grupo);
            try {
                modelo.create(alumno);
            } catch (IOException ex) {
                vista.mostrarError("Error de Entrada/Salida");
            }
        } else {
            vista.mostrarError("Error. Grupo no encontrado");
        }
    }

    private void actualizar() throws IOException {

        String id = vista.getId();
        alumno = vistaAlumno.obtener();
        alumno.setId(id);
        grupo = getGrupo(alumno.getGrupo().getId());

        if (grupo != null) {
            alumno.setGrupo(grupo);
            try {
                modelo.update(alumno);
            } catch (IOException ex) {
                vista.mostrarError("Error de Entrada/Salida");
            }
        } else {
            vista.mostrarError("Error. Grupo no encontrado");
        }
    }

    private void borrar() throws IOException {
        String id = vista.getId();
        alumno = new Alumno();
        alumno.setId(id);
        try {
            modelo.delete(alumno);
        } catch (IOException ex) {
            vista.mostrarError("Error de Entrada/Salida");
        }
    }
}
